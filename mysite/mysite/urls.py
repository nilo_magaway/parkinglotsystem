from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

from parking.views import *

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', log_in),
    url(r'^login/$', log_in),
    url(r'^logout/$', log_out),
    url(r'^main/$', main),
    url(r'^register/$', newuser),
)
