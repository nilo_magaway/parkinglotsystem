from django.db import models

# Create your models here.

class Parking(models.Model):
    description = models.TextField(default='')

class Log(models.Model):
    Date = models.DateField()
    Time = models.TimeField()
    parking = models.ForeignKey(Parking)

