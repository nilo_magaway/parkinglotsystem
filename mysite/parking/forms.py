from django import forms
from .models import *


class LoginForm(forms.Form):
    username = forms.CharField(max_length=30,widget=forms.TextInput(attrs={'placeholder': 'Username',}), label = '',error_messages={'required': 'Please enter your username'})
    password = forms.CharField(max_length=30, label='', widget=forms.PasswordInput(attrs={'placeholder': 'Password'}),error_messages={'required': 'Please enter your password'})

class SignUpForm(forms.Form):
    username = forms.CharField(max_length=30,widget=forms.TextInput(attrs={'placeholder': 'Username',}), label = '')
    password = forms.CharField(max_length=30, widget=forms.PasswordInput(attrs={'placeholder': 'Password'}), label='')
    repeat_password = forms.CharField(max_length=30, widget=forms.PasswordInput(attrs={'placeholder': 'Repeat Password'}), label='')
    first_name = forms.CharField(max_length=30,widget=forms.TextInput(attrs={'placeholder': 'First Name',}), label = '')
    last_name = forms.CharField(max_length=30,widget=forms.TextInput(attrs={'placeholder': 'Last Name',}), label = '')
    email = forms.EmailField(widget=forms.TextInput(attrs={'placeholder': 'Email',}), label = '')
