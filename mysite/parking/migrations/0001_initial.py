# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Parking'
        db.create_table(u'parking_parking', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('django.db.models.fields.TextField')(default='')),
        ))
        db.send_create_signal(u'parking', ['Parking'])

        # Adding model 'Log'
        db.create_table(u'parking_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Date', self.gf('django.db.models.fields.DateField')()),
            ('Time', self.gf('django.db.models.fields.TimeField')()),
            ('parking', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['parking.Parking'])),
        ))
        db.send_create_signal(u'parking', ['Log'])


    def backwards(self, orm):
        # Deleting model 'Parking'
        db.delete_table(u'parking_parking')

        # Deleting model 'Log'
        db.delete_table(u'parking_log')


    models = {
        u'parking.log': {
            'Date': ('django.db.models.fields.DateField', [], {}),
            'Meta': {'object_name': 'Log'},
            'Time': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parking': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['parking.Parking']"})
        },
        u'parking.parking': {
            'Meta': {'object_name': 'Parking'},
            'description': ('django.db.models.fields.TextField', [], {'default': "''"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['parking']