from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from .models import *
from .forms import *
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required


# Create your views here.

def newuser(request):
    error = None
    if request.user.is_authenticated():
        return HttpResponseRedirect('/main/')
    if request.method == 'POST':  # If the form has been submitted...
        form = SignUpForm(request.POST)  # A form bound to the POST data
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            repeat_password = form.cleaned_data['repeat_password']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            if password==repeat_password:
                newuser = User.objects.create_user(username = username, password = password, email = email)
                newuser.first_name = first_name
                newuser.last_name = last_name
                newuser.save( )
            else:
                error = 'Unmatched Password!'
                return render(
                request,
                'newuser.html',
                {'form': form, 'error': error},
                )
            user = authenticate(username = username, password = password)
            if user is not None:
                login(request,user)
                return HttpResponseRedirect('/main/')
            # send_mail(subject, message, sender, recipients)
            return HttpResponseRedirect("/main/")
    else:
        form = SignUpForm()

    return render(
        request,
        'signup.html',
        {'form': form}
    )

def log_in(request):
    message = ""
    if request.user.is_authenticated():
        return HttpResponseRedirect('/main/')

    if request.method == 'POST':  # If the form has been submitted...
        form = LoginForm(request.POST)  # A form bound to the POST data
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)

            if user is not None:
                login(request,user)
                return HttpResponseRedirect('/main/')
            else:
                message = "Incorrect Details!"

            return render(request,'login.html',{'form':form,'message':message})

    else:
        form = LoginForm()

    return render(request,'login.html',{'form':form,'message':message})

def log_out(request):
    if request.user.is_authenticated():
        logout(request)

    # html = "<html><body>Logged out. <br><a href='/login/'> Login </a></body></html>"

    return HttpResponseRedirect('/login/')

@login_required
def main(request):
    return render(request, 'main.html',{'user': request.user.first_name})
